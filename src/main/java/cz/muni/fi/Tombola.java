package cz.muni.fi;

import java.io.OutputStream;
import java.util.Set;

public interface Tombola {
    
    /**
     * Returns tombola prizes sorted by their number.
     * @return tombola prizes sorted by their number.
     */
    Set<TombolaPrize> getPrizesSortedByNumber();
    
    /**
     * Returns tombola prizes sorted by their price.
     * @return tombola prizes sorted by their price.
     */
    Set<TombolaPrize> getPrizesSortedByPrice();
    
    /**
     * Returns tombola prizes of given donor.
     * @param donor Donor. Null value represents anonymous donors.
     * @return tombola prizes of given donor or anonymous donors, empty set if the donor does not exist.
     */
    Set<TombolaPrize> getPrizesOfDonor(String donor);
    
    /**
     * Gives the prize, i.e. removes the prize from tombola.
     * @param number Prize number
     * @return The prize or null if there is no prize under the given number in tombola.
     */
    TombolaPrize givePrize(int number);
            
    /**
     * Writes list of tombola prizes sorted by their numbers.
     * @param os Output stream
     * @throws TombolaException on any I/O failure
     */
    void printListOfPrizes(OutputStream os) throws TombolaException;
}
