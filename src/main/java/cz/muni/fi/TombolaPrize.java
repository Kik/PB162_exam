/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi;

//import java.util.HashSet;
//import java.util.Set;

/**
 *
 * @author Kristina Miklasova, 4333 83
 */
public class TombolaPrize implements Comparable<TombolaPrize>{
    
    private String name;
    private String donor;
    private Double price;
    private int number; 
    private static int num = 0;
    //private Set<TombolaPrize> tombola = new HashSet<>();
            
    public TombolaPrize(String name, String donor, Double price) throws TombolaException {
        
        if ((name == null) || (price == null)){
            throw new TombolaException("wrong arguments");
        }
        if (price < 0){
            throw new TombolaException("wrong price");
        }
        this.name = name;
        if (donor == null){
            this.donor = null;
        } else {
            this.donor = donor;
        }   
        this.price = price;
        number = ++num;
  //      tombola.add(this);
  //      this.number = tombola.size();
    }
    
    public String getName() {
        return name;
    }

    public String getDonor() {
        return donor;
    }

    public Double getPrice() {
        return price;
    }

    public int getNumber() {
        return number;
    }
    
    public String toString(){
        if (getDonor() == null){
            return getNumber() + ". " + getName();
        }
        return getNumber() + ". " + getName() + " - " + getDonor();
    }
    
    public boolean equals(Object obj){
        if (!(obj instanceof TombolaPrize)){
            return false;
        }
        TombolaPrize tmb = (TombolaPrize) obj;
        
        return this.number == tmb.getNumber();
    }

    @Override
    public int compareTo(TombolaPrize t) {
        return this.number - t.getNumber();
    }
    
    
    
}
