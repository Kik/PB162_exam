/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import static java.util.Collections.unmodifiableSet;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 *
 * @author Kristina Miklasova, 4333 83
 */
public class TombolaImpl implements Tombola{

    private Set<TombolaPrize> tmbSet = new HashSet<>();
    
    public TombolaImpl(File file) throws TombolaException{
        
        try (BufferedReader br = new BufferedReader(new FileReader(file))){
            String line = br.readLine();
            while (line != null){
                String[] tokens = line.split("[;]");  // tokens[0] = donor
               
                if (tokens.length < 2){
                    throw new TombolaException("no info about prize");
                }
                
                if (tokens[0].equals("")) {
                        tokens[0] = null;
                    }
                
                for (int i=1; i<(tokens.length); i++){
                    String[] prices = tokens[i].split("[:]");  // prices[0] = name; prices[1] = price
                    if (prices.length != 2){
                        throw new TombolaException("there must be info about name and price of prize");
                    }
                     
                    tmbSet.add(new TombolaPrize(prices[0], tokens[0], Double.valueOf(prices[1])));
                }
                line = br.readLine();
            }       
        } catch (IOException ex){
            throw new TombolaException("reading fail");
        } 
    }
    
    @Override
    public Set<TombolaPrize> getPrizesSortedByNumber() {
        SortedSet<TombolaPrize> set = new TreeSet<>();
        
        set.addAll(tmbSet);
               
        return set;
    }

    @Override
    public Set<TombolaPrize> getPrizesSortedByPrice() {
        SortedSet<TombolaPrize>  set = new TreeSet<>(new TombolaComparator());
        
        set.addAll(tmbSet);
        
        return set;
    }

    @Override
    public Set<TombolaPrize> getPrizesOfDonor(String donor) {
        Set<TombolaPrize> set = new HashSet<>();
        
        if (donor == null){
            for(TombolaPrize tp : tmbSet){
                if (tp.getDonor() == null){
                    set.add(tp);
                }
            }
        } else {
            for (TombolaPrize tp : tmbSet){
                if (tp.getDonor() != null && tp.getDonor().equals(donor)){
                    set.add(tp);
                }
            }
        }
        
        return set;
    }

    @Override
    public TombolaPrize givePrize(int number) {
        TombolaPrize givenPrize = null;
        
        for(TombolaPrize tp : tmbSet){
            if (tp.getNumber() == number){
                givenPrize = tp;
            }
        }
        if (givenPrize != null){
            tmbSet.remove(givenPrize);
        }
        
        return givenPrize;
    }

    @Override
    public void printListOfPrizes(OutputStream os) throws TombolaException {
        Set<TombolaPrize> sortByNum = new HashSet<>();
        sortByNum = this.getPrizesSortedByNumber();
        
//        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os))){
        try {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
            for(TombolaPrize tp : sortByNum){
                bw.write(tp.toString());
                bw.newLine();
            }
            bw.flush();
            
        } catch (IOException ex){
            throw new TombolaException("writing fail");
        }   
    }
   
}
