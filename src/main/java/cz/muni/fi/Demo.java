package cz.muni.fi;

import java.io.File;

public class Demo {

    public static void main(String[] args) throws TombolaException {
        System.out.println("READING PRIZES FROM FILE...");
        TombolaImpl tombola = new TombolaImpl(new File("tombola-in.txt"));
        
        System.out.println("COMPLETE LIST OF PRIZES:");
        tombola.printListOfPrizes(System.out);
        
        System.out.println("GIVING PRIZE No. 2");
        tombola.givePrize(2);
        
        System.out.println("PRIZES SORTED BY THEIR PRICE:");
        for (TombolaPrize prize: tombola.getPrizesSortedByPrice()) {
            System.out.println(prize);
        }
        
        System.out.println("PRIZES OF THE DONOR \"Bohata firma, s.r.o.\":");
        for (TombolaPrize prize: tombola.getPrizesOfDonor("Bohata firma, s.r.o.")) {
            System.out.println(prize);
        }
        
        System.out.println("PRIZES OF ANONYMOUS DONORS:");
        for (TombolaPrize prize: tombola.getPrizesOfDonor(null)) {
            System.out.println(prize);
        }
        
        System.out.println("PRIZES OF NON-EXISTING DONOR:");
        for (TombolaPrize prize: tombola.getPrizesOfDonor("bla bla")) {
            System.out.println(prize);
        }
    }
}
