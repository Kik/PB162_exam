/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi;

import java.util.Comparator;

/**
 *
 * @author Kristina Miklasova, 4333 83
 */
public class TombolaComparator implements Comparator<TombolaPrize>{

    @Override
    public int compare(TombolaPrize t1, TombolaPrize t2) {
       if (t1.getPrice() > t2.getPrice()){
           return -1;
       } else if (t1.getPrice() < t2.getPrice()){
           return 1;
       } else {
           return 0;
       }
    }   
}
