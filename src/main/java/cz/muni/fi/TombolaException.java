package cz.muni.fi;

/**
 *
 * @author Kristina Miklasova, 4333 83
 */
public class TombolaException extends Exception{

    public TombolaException() {
        super();
    }

    public TombolaException(String msg) {
        super(msg);
    }

    public TombolaException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public TombolaException(Throwable cause) {
        super(cause);
    }

    
}
